import React, {useEffect} from 'react';
import { If, Then, Else} from 'react-if'

import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import ListGroup from 'react-bootstrap/ListGroup'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import CardColumns from 'react-bootstrap/CardColumns'

import { BlockPicker, CirclePicker } from 'react-color';

import useLocalStorage from './useLocalStorage';
import Luxafor from './luxafor';

function App() {
  const [luxaforUserId, setLuxaforUserId] = useLocalStorage('luxaforUserId', null);
  let luxafor = luxaforUserId ? new Luxafor(luxaforUserId) : null;

  useEffect(() => {
    const { location } = window;
    const { search } = location;
    const searchParams = new URLSearchParams(search);
    const luxaforUserId = searchParams.get('luxaforUserId');
    if (luxaforUserId) {
      setLuxaforUserId(luxaforUserId);
      window.location = window.location.pathname;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    const { target } = event;
    const data = new FormData(target);
    setLuxaforUserId(data.get('userId'))
  }

  const setCustom = (color, event) => {
    luxafor.solid_color(color[color.source]);
  }

  // Uses onChange (in contrast to onChangeComplete) to get event.target.title
  const setBlink = (color, event) => {
    const { target } = event;
    const { title } = target;
    luxafor.blink(title);
  }

  const setPattern = (name) => (event) => {
    luxafor.pattern(name);
  }

  return (
    <div className="App">
      <Navbar>
        <Navbar.Brand>
          <a href="/">Luxafor UI</a>
        </Navbar.Brand>
      </Navbar>
      <Container>
          <Row>
            <Col>
              <CardColumns>
                <If condition={luxaforUserId == null}>
                  <Then>
                    <Card>
                      <Card.Body>
                        <Card.Title>Luxafor userId</Card.Title>

                        <Form onSubmit={handleSubmit} inline>
                          <Form.Control name="userId" placeholder="00000000" />
                          <Button type="submit">Submit</Button>
                        </Form>
                      </Card.Body>
                    </Card>
                  </Then>
                  <Else>
                    <Card>
                      <Card.Body>
                        <Card.Title>Off</Card.Title>
                        <CirclePicker colors={['black']} onChangeComplete={setCustom} />
                      </Card.Body>
                    </Card>
                    <Card>
                      <Card.Body>
                        <Card.Title>Solid custom color</Card.Title>
                        <BlockPicker colors={Luxafor.base_colors} triangle='hide' onChangeComplete={setCustom}/>
                      </Card.Body>
                    </Card>
                    <Card>
                      <Card.Body>
                        <Card.Title>Blink</Card.Title>
                        <CirclePicker colors={Luxafor.base_colors} width={200} onChange={setBlink} />
                      </Card.Body>
                    </Card>
                    <Card>
                      <Card.Body>
                        <Card.Title>Pattern</Card.Title>
                        <ListGroup>
                          {Luxafor.patterns.map(pattern => {
                            return (
                              <ListGroup.Item
                                key={pattern}
                                action
                                onClick={setPattern(pattern)}>
                                  {pattern}
                              </ListGroup.Item>
                            );
                          })}
                        </ListGroup>
                      </Card.Body>
                    </Card>
                  </Else>
                </If>
              </CardColumns>
            </Col>
          </Row>
      </Container>
    </div>
  );
}

export default App;
