const base_url = '/api';

const patterns = ["police", "traffic lights", "random 1", "random 2", "random 3", "random 4", "random 5"];
const base_colors = ["red", "green", "yellow", "blue", "white", "cyan", "magenta"];

export default class Luxafor {
  constructor(userId) {
    this.userId = userId;
  }

  solid_color(hex) {
    const url = `${base_url}/solid_color`;
    const custom_color = hex.slice(1).toLowerCase();
    const data = {
      userId: this.userId,
      actionFields:{
        color: "custom",
        custom_color,
      }
    }
    return this.fetchHelper(url, data);
  }

  blink(color) {
    if (!base_colors.includes(color)) {
      console.error('Blink called with non-supported color', color);
      return;
    }
    const url = `${base_url}/blink`;
    const data = {
      userId: this.userId,
      actionFields: {
        color,
      }
    }
    return this.fetchHelper(url, data);
  }

  pattern(name) {
    const url = `${base_url}/pattern`;
    const data = {
      userId: this.userId,
      actionFields: {
        pattern: name,
      }
    }
    return this.fetchHelper(url, data);
  }

  fetchHelper(url, data) {
    return fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
  }
}

Luxafor.patterns = patterns;
Luxafor.base_colors = base_colors;
